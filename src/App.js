import { VRCanvas , DefaultXRControllers} from '@react-three/xr'
import { Canvas } from '@react-three/fiber'
import { Sky, PointerLockControls } from "@react-three/drei"
import { Physics } from "@react-three/cannon"

export default function App() {
    return (
        <div style={{ position: "absolute", width: '100%', height: '100%' }}>
            <VRCanvas
                shadowMap
                gl={{
                    alpha: false,
                    antialias: true,
                    depth: true,
                    stencil: true,
                    premultipliedAlpha: true,
                    preserveDrawingBuffer: true,
                    powerPreference: "high-performance",
                    failIfMajorPerformanceCaveat: true,
                }}
                camera={{ fov: 45 }}
            >
                <Sky sunPosition={[100, 20, 100]} />
                <ambientLight intensity={0.3} />
                <pointLight castShadow intensity={0.8} position={[100, 100, 100]} />
                <Physics gravity={[0, -30, 0]}>
                    <mesh>
                        <boxGeometry attach="geometry" args={[1, 2, 2]} />
                        <meshStandardMaterial attach="material" color="blue"/>
                    </mesh>
                </Physics>
                <DefaultXRControllers />
                <PointerLockControls />
            </VRCanvas>
            </div>

    )
}

